<?php
/** 
 *  В данном конфигурационном файле описываются данные для доступа к базе
 *  и пути для конфигов
 **/
 
/** Старт сессии, файл должен быть сохранен без DOM информации **/
session_start();

include_once 'module.php';

/** Переменные **/
$slash = "/";
$apache_conf_dir = '/home/hosting/configs/apache2/'; //Директория с конфигами апача
$nginx_conf_dir = '/home/hosting/configs/nginx/'; //Директория с конфигами енджинкса
$user = 'hosting'; //Юзер, от которого запускается апач
$group = 'hosting'; //Группа, от которой запускается апач
$work_dir = '/home/hosting/users'; //Рабочая директория, в которой находятся корневые директории пользователей

/** Параметры потключения к бд **/
$db_host = 'localhost';
$db_login = 'hosting';
$db_passwd = 'gfhfghgfh';
$db_name = 'hosting';

/** подключаемся к бд **/
$db = new mysql(); /** Создаем новый объект класса **/
$db -> connect($db_host, $db_login, $db_passwd, $db_name);
?>
