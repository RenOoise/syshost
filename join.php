<?php
include_once 'conf.php';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="ico/favicon.png">

    <title>Join / sysHoster</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

<?php
$reg = new auth();  /** Создаем новый объект класса **/
$form = '
<div class="row">
<div class="container">
 <div class="col-md-4">
 </div>
<div class="col-md-4">
      <form role="form" action="" method="post">

        <h3 class="form-signin-heading">Заполните следующие поля</h3>
		<div class="form-group">
		    <input type="text" name="loginr" id="" value="'.@$_POST['login'].'" class="form-control-reg" placeholder="Логин">
			<input type="password" name="passwd1" id="" class="form-control-reg" placeholder="Пароль">
			<input type="password" name="passwd2" id="" class="form-control-reg" placeholder="Повторите пароль">
			<input type="email" name="mail" value="'.@$_POST['mail'].'" class="form-control-reg" placeholder="Email">
	    </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit" value="send" name="send">Зарегистрироваться</button>
      </form>

    </div> 
	<div class="col-md-4">
	</div>
	</div>
	<!-- /container -->
</div>
 	';
if (isset($_POST['send'])) {
	if ($reg->reg($_POST['loginr'], $_POST['passwd1'], $_POST['passwd2'], $_POST['mail'])) {
		print '
			<h2>Регистрация успешна.</h2>
			Теперь вы можете <a href="/">войти</a>.
		';
	} else print $form;
} else print $form;


//var_dump($_POST);
?>
</body>
</html>
