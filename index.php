<?php
include_once 'conf.php';
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/ico/favicon.png">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
	<link href="css/cat.css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->

<?php
$r='';

$auth = new auth(); /** Создаем новый объект класса **/

/** Авторизация **/
if (isset($_POST['send'])) {
	if (!$auth->authorization()) {
		$error = $_SESSION['error'];
		unset ($_SESSION['error']);
	}
}

/** выход **/
if (isset($_GET['exit'])) $auth->exit_user();

/** Проверка авторизации **/
if ($auth->check()) { 
	$r.='<title>Main / sysHoster</title>
		</head>
		<body><div class="container">
		<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-6">
		Добро пожаловать, '.$_SESSION['login_user'].'<br/>
		Ты сейчас находишься на разрабатываемом сервисе по предоставлению хостинга для веб-сайтов.<br/>
		<a href="create">Регистрация хостов</a>, правда, еще не работает, но зато у нас УЖЕ есть котики.<br/>
		<br/>
		Лучше <a href="?exit">выйди</a> от сюда, чтобы не расстраивать свою психику.
		</div>
		<div class="col-md-3">
		</div>
		<div class="grumpy"> </div>
		</div> <!-- /container -->';
} elseif  (isset($error)) {
	$r.=$error.'<a href="recovery">Восстановить пароль</a><br/>';
}
 else {
	$r.='<title>Login / sysHoster</title>
		</head>
		<body><div class="container">
		<form action="" method="post" class="form-signin">
        <h3 class="form-signin-heading">Пожалуйста, войдите</h3>
        <input type="text" name="login" value="'.@$_POST['login'].'" class="form-control" placeholder="Логин" autofocus>
        <input type="password" name="passwd" id="" class="form-control" placeholder="Пароль">
		
<button value="send" name="send" class="btn btn-lg btn-primary  btn-block">Войти</button>
      </form>
       <form method="POST" action="join.php" class="form-signin">
	<button class="btn btn-lg btn-default btn-block" name="register" type="submit">Зарегистрироваться</button>
      </form>
<div class="cat"> </div>
    </div> <!-- /container -->';
}
	print $r;
?>
</body>
</html>
