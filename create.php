<?php
include_once 'conf.php';
$r='';

$auth = new auth(); /** Создаем новый объект класса **/

/** Авторизация **/
if (isset($_POST['send'])) {
	if (!$auth->authorization()) {
		$error = $_SESSION['error'];
		unset ($_SESSION['error']);
	}
}

/** выход **/
if (isset($_GET['exit'])) $auth->exit_user();

/** Проверка авторизации **/
if ($auth->check()) { 
$r.='
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="ico/favicon.png">

    <title>Create Host / sysHoster</title>
	
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/signin.css" rel="stylesheet">
	<link href="css/cat.css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>  

<div class="container">
 <div class="row">
 <div class="col-md-4">
 </div>
<div class="col-md-4">
      <form role="form" action="create.act.php" method="post">

        <h3 class="form-signin-heading">Заполните следующие поля</h3>
		<div class="form-group">
		    <input type="text" name="inputFirstName" id="" class="form-control-reg" placeholder="Имя">
			<input type="text" name="inputLastName" id="" class="form-control-reg" placeholder="Фамилия">
			<input type="text" name="inputDomain" id="" class="form-control-reg" placeholder="Доменное имя">
	    </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit" value="send" name="create">Создать</button>
      </form>

    </div> 
	<div class="col-md-4">
	</div>

	</div>
	<div class="cage"> </div>
</div>	<!-- /container -->
  </body>
</html>';
}
else {
header("Location: /");
} 
	print $r;
?>
