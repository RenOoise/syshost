<?php
/**
 * Экшн, выполняемый при регистрации домена на сервере
 **/
include ("conf.php");

/** Переводим переменные из форм в более юзабильный вид **/
//$CustomerFirstName = $_POST["inputFirstName"];
//$CustomerLastName = $_POST["inputLastName"];
$CustomerLogin= $_SESSION['login_user'];
$CustomerDomain = $_POST["inputDomain"];

if(isset($_POST['create'])) {

	$apacheConfName = $apache_conf_dir.$CustomerDomain;
	$nginxConfName = $nginx_conf_dir.$CustomerDomain;

	if (file_exists($apacheConfName)) { /** Проверяем, существует ли конфигурационный файл для apache с таким именем **/
		echo "This domain is alredy registred. (apache)"; /** Если существует, выдаем ошибку **/
	} else {
		apacheCreate ($apache_conf_dir, $CustomerDomain, $work_dir, $CustomerLogin); /** Если не существует, вызываем функцию, которая его создаст **/
	}
	if (file_exists($nginxConfName)) { /** Проверяем, существует ли конфигурационный файл для nginx с таким именем **/
		echo "This domain is alredy registred. (nginx)"; /** Если существует, выдаем ошибку **/
	} else {
		nginxCreate ($nginx_conf_dir, $CustomerDomain, $work_dir, $CustomerLogin); /** Если не существует, вызываем функцию, которая его создаст **/
		createDocDir ($work_dir,$CustomerLogin,$CustomerDomain);
	}
}
?>
